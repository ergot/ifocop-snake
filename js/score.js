/**
 * Give a game object
 * @param {string} id - the css id of the score tag
 * @returns {ScoreFactory} Return a object game
 */
function getScore(id) {
  function ScoreFactory() {
    this.score = 0;
    this.step = 1;
    this.isPlaying = true;
    this.addScore = function () {
      if (this.isPlaying) {
        this.score = this.score + this.step;
      }
    };
    /**
     * function anonyme: get score tag html
     */
    this.dom = (function () {
      return document.getElementById(id);
    })();
    this.printScore = function () {
      this.dom.innerHTML = this.score;
    };
    this.reload = function () {
      this.score = 0;
      this.isPlaying = true;
      this.printScore();
    };
  }
  return new ScoreFactory();
}
