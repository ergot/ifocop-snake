/**
 * Use to manage animation in terms of keyboard
 * @param {GpuFactory}               gpu - a gpu object
 * @param {SnakeFactory}             snake - a snake object
 * @param {CollisionFactory}         collision - a object to compute the collision
 * @param {CanvasFactory}            canvas -  a canvas object
 * @param {AppleFactory}             apple - a apple object
 * @param {ScoreFactory}             score - a score object
 * @param {TimerFactory}             timer - a timer object
 * @returns {AnimationManagerFactory} Return a AnimationManager
 */
function getAnimationManager(gpu, snake, collision, canvas, apple, score, timer) {
  window.requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
    window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;

  let cancelAnimationFrame = window.cancelAnimationFrame || window.mozCancelAnimationFrame;

  function AnimationManagerFactory() {
    /**
     * Stock the last id of animation frame
     * @type {number}
     */
    this.lastAnimationFrame = 0;
    /**
     * A array of functions
     * @type {Array}
     */
    this.bag = [];
    /**
     * Ratatatatatattaata
     * @return {function} buildCatling - this function is used to loop in request animation frame
     */
    this.buildCatling = function () {
      let animationManager = this;
      return function catling() {
        animationManager.bag.forEach(function (element) {
          element();
        });
        animationManager.lastAnimationFrame = window.requestAnimationFrame(catling);
      };
    };
    /**
     * The entry point of animation framme loop
     * @param  {string} direction - left, right, go see keyboardManager.mapping
     * @return {void}
     */
    this.run = function (direction) {
      let snakeMove = this.builderSnakeMove(direction);
      this.bag.push(snakeMove);
      this.lastAnimationFrame = window.requestAnimationFrame(this.buildCatling());
    };
    /**
     * Stop the animation frame
     * @return {void}
     */
    this.stop = function () {
      cancelAnimationFrame(this.lastAnimationFrame);
      this.bag = [];
    };
    /**
     * Build a snake move object
     * @param {string} direction  - check keyboardManager.mapping
     * @return {Function} - a snake move object
     */
    this.builderSnakeMove = function (direction) {
      /**
       * Build a object compatible with  Collision.hasCollision
       * @returns {{style: {width: number, height: number}}} the Object
       */
      function getTransformerTail() {
        return {
          style: {
            width: snake.style.width,
            height: snake.style.height
          }
        };
      }
      /**
       * Detect if a object touch the tail of snake
       * @param   {object}  obj - Obj can be anything, compatible with collision.hasCollision
       * @returns {boolean}     - True if touch, False if not
       */
      function collisionWithTail(obj) {
        let transformerTail = getTransformerTail();
        let tail = snake.getTail();
        for (let i = 1; i < tail.length; i++) {
          transformerTail.position = tail[i];
          if (collision.hasCollision(obj, transformerTail)) {
            return true;
          }
        }
        return false;
      }

      /**
       * Detect if a object touch the position history of snake.
       * It s used when i need to pop the apple, don t use collisionWithTail because the history is cutted
       * @param {object} obj - Obj can be anything, compatible with collision.hasCollision
       * @returns {boolean} - True if touch, False if not
       */
      function collisionWithSnakePositionHistory(obj) {
        let transformerTail = getTransformerTail();
        let tail = snake.positionHistory;
        for (let i = 1; i < tail.length; i++) {
          transformerTail.position = tail[i];
          if (collision.hasCollision(obj, transformerTail)) {
            return true;
          }
        }
        return false;
      }

      // the core of snake animation
      return function () {
        gpu.clearSnake();
        gpu.clearSnakeTail();

        if (score.isPlaying && timer.isPlaying) {
          snake.move[direction]();
          snake.recordPosition(snake.position.x, snake.position.y);
        }

        if (collision.hasCollision(snake, apple)) {
          snake.setEat();
          score.addScore();
          score.printScore();
          gpu.clearApple();
          apple.newPosition();
          // collision with head of snake
          while (collision.hasCollision(snake, apple)) {
            apple.newPosition();
          }
          // collision with tail snake
          while (collisionWithSnakePositionHistory(apple)) {
            apple.newPosition();
          }
          gpu.drawApple();
        }

        // snake se mort la queue
        if (snake.eat > 1) {
          if (collisionWithTail(snake)) {
            score.isPlaying = false;
            timer.stop();
          }
        }

        if (!collision.inCanvas(snake, canvas)) {
          timer.stop();
          score.isPlaying = false;
          gpu.drawSnake();
          gpu.drawSnakeTail();
        } else {
          gpu.drawSnake();
          gpu.drawSnakeTail();
        }
      };
    };
  }
  return new AnimationManagerFactory();
}
