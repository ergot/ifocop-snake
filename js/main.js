'use strict';
/* eslint-disable func-names */





/**
 * The beginning ...
 */
window.addEventListener('load', function () {
  let score = getScore('score');
  score.printScore();

  let timer = getTimer('timer');
  timer.printTime();

  let canvas = getCanvas('gamvas');
  let snake = getSnake(canvas);
  let apple = getApple(canvas);
  let gpu = getGpu(canvas, snake, apple);
  let collision = getCollisionEngin();

  gpu.drawSnake();
  while (collision.hasCollision(snake, apple)) {
    apple.newPosition();
  }
  gpu.drawApple();

  let animationManager = getAnimationManager(gpu, snake, collision, canvas, apple, score, timer);

  let keyboardManager = getKeyboardManager(animationManager, timer);

  listenerKeyboard(keyboardManager);

  window.addEventListener('keydown', function (e) {
    if (e.keyCode === 32) {
      gpu.clearSnake();
      gpu.clearSnakeTail();
      gpu.clearApple();

      snake.reload();
      apple.reload();
      score.reload();
      timer.reload();
      while (collision.hasCollision(snake, apple)) {
        apple.newPosition();
      }

      gpu.drawSnake();
      gpu.drawApple();

      keyboardManager.reload();
    }
  });
});
