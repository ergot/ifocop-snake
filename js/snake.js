/**
 * Get a snake object
 * @param {CanvasFactory}   canvas - A canvas object
 * @returns {SnakeFactory}  Return a snake object
 */
function getSnake(canvas) {
  let canvasXmax = canvas.style.width;
  let canvasYmax = canvas.style.height;

  /**
   * Get a random position between two number
   * @param   {Number}  canvasSize  Can be height or width.
   * @param   {Number}  snakeSize   Height or width of the snake style.
   * @returns {Number}              Return a integer
   */
  function getSnakePositionRandom(canvasSize, snakeSize) {
    let max = canvasSize - snakeSize;
    return getRandomNumber(max);
  }

  /**
   * Build a dynamic function for add px to the snake
   * @param {number}      position - can be x or y coordonne
   * @returns {Function}  Return a function
   */
  function addPx(position) {
    let snake = this;
    return function () {
      snake.position[position] += snake.params.speed;
    };
  }

  /**
   * Build a dynamic function for remove px to the snake
   * @param {number}      [position] - can be x or y coordonne
   * @returns {Function}  Return a function
   */
  function rmvPx(position) {
    let snake = this;
    return function () {
      snake.position[position] -= snake.params.speed;
    };
  }

  /**
   * The snake factory
   * @constructor
   */
  function SnakeFactory() {
    /**
     * To config the snake
     * @type {{speed: number}}
     */
    this.params = {
      speed: 3
    };
    /**
     * Style of snake
     * @type {{height: number, width: number, color: string}}
     */
    this.style = {
      height: 10,
      width: 10,
      color: '#2c2c2c'
    };
    /**
     * Init position with a random value
     * @type {{x: Number, y: Number}}
     */
    this.position = {
      x: getSnakePositionRandom(canvasXmax, this.style.width),
      y: getSnakePositionRandom(canvasYmax, this.style.height)
    };
    /**
     * Records the position of the snake head
     * @type {Array}
     */
    this.positionHistory = [];
    /**
     * Number of times where he ate an apple
     * @type {number}
     */
    this.eat = 0;
    /**
     * Allows to change the position of the snake
     * @type {{left: *, top: *, right: *, down: *}}
     */
    this.move = {
      left: rmvPx.call(this, 'x'),
      top: rmvPx.call(this, 'y'),
      right: addPx.call(this, 'x'),
      down: addPx.call(this, 'y')
    };
    /**
     * Gives a random position to the snake
     * @returns {void}
     */
    this.randomPosition = function () {
      this.position.x = getSnakePositionRandom(canvasXmax, this.style.width);
      this.position.y = getSnakePositionRandom(canvasYmax, this.style.height);
    };
    /**
     * Saves the position of the snake.
     * The last known position is recorded at the beginning of the array
     * @param {number} x - x coordonnee
     * @param {number} y - y coordonnee
     * @returns {void}
     */
    this.recordPosition = function (x, y) {
      this.positionHistory.unshift({x: x, y: y});
    };
    /**
     * Returns an array of tail
     * @returns {Array} tail - Returns an array with the positions of the tail
     */
    this.getTail = function () {
      /**
       * Stock each position of the tail
       * @type {Array}
       */
      let tail = [];
      /**
       * Interval between two positions
       * @type {number}
       */
      let step = 4;
      let j = step;
      for (let i = 0; i < this.eat; i++) {
        // When apple pop just in front of the head of sneake, positionHistory need more recording
        // eslint-disable-next-line no-undefined
        if (this.positionHistory[j] !== undefined) {
          tail.push(this.positionHistory[j]);
        }
        j += step;
      }
      // clean history with old position
      this.positionHistory.splice(j);
      return tail;
    };
    /**
     * Set eat property
     * @returns {void}
     */
    this.setEat = function () {
      this.eat += 1;
    };
    /**
     * Reload the snake
     * @returns {void}
     */
    this.reload = function () {
      this.positionHistory = [];
      this.eat = 0;
      this.randomPosition();
    };
  }
  return new SnakeFactory();
}
