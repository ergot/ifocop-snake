/**
 * Give a timer object
 * @param {string}          id Add the id of the html tag
 * @returns {TimerFactory}  Return a Timer object
 */
function getTimer(id) {
  function TimerFactory() {
    this.params = { defaultSecond: 120};
    this.second = this.params.defaultSecond;
    this.isPlaying = true;
    this.dom = (function () {
      return document.getElementById(id);
    })();
    /**
     * Converts seconds to percent for tag
     * @return {string} return a number
     */
    this.getTimeInPercentage = function () {
      let percentage = ( this.second / this.params.defaultSecond) * 100;
      return percentage.toFixed(2);
    };
    /**
     * Print time in the progress bar tag
     * @return {void}
     */
    this.printTime = function () {
      this.dom.style.width = this.getTimeInPercentage() + '%';
    };
    this.idSetInterval = Number();
    /**
     * Run the timer
     * recursive function: call every second himself
     * @return {void}
     */
    this.run = function () {
      this.idSetInterval = setInterval(function (timer) {
        timer.second -= 1;
        if (timer.second >= 1 ) {
          timer.printTime();
        } else {
          timer.printTime();
          timer.stop();
          timer.isPlaying = false;
        }
      }, 1000, this);
    };
    /**
     * Stop the timer
     * @return {void}
     */
    this.stop = function () {
      clearInterval(this.idSetInterval);
    };
    /**
     * Reset the object
     * @return {void}
     */
    this.reload = function () {
      this.stop();
      this.isPlaying = true;
      this.second = this.params.defaultSecond;
      this.printTime();
    };
  }
  return new TimerFactory();
}
