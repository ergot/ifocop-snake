/**
 * Return a Apple object
 * @param {CanvasFactory}  canvas A canvas factory object
 * @return {AppleFactory} Return a apple object
 */
function getApple(canvas) {
  function AppleFactory() {
    /**
     * Define the style of the apple
     * @type {{height: number, width: number, color: string}}
     */
    this.style = {
      height: 10,
      width: 10,
      color: '#ea0b29'
    };
    /**
     * Init a random position
     * @type {{x: number, y: number}}
     */
    this.position = {
      x: getRandomNumber((canvas.style.width  - this.style.height)),
      y: getRandomNumber((canvas.style.height - this.style.width))
    };
    /**
     * Define a new random position of the apple
     * @return {void}
     */
    this.newPosition = function () {
      this.position.x = getRandomNumber((canvas.style.width  - this.style.height));
      this.position.y = getRandomNumber((canvas.style.height - this.style.width));
    };
    /**
     * Reload / reset the apple object
     * @return {void}
     */
    this.reload =  function () {
      this.newPosition();
    };
  }
  return new AppleFactory();
}